<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
  }

  public function index()
  {
    $this->load->view('home/index');
  }

  public function about()
  {
    $this->load->view('home/about');
  }

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */