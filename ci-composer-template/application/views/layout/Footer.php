<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Footer</title>
  <link rel="stylesheet" href="">
  <style type="text/css">
    body{
      padding: 0;
      margin: 0;
      background-color: #5CC29F;
      height: 630px;
    }
    #footer{
      background-color: #4E4E4E;
      height: 60px;
      margin-top: 59px;
    }
    #footer p{
      color: white;
      text-align: center;
      font-size: 15px;
      padding: 20px;
    }
    footer{
      background-color: #940d11;
      clear: both;
      height: 600px;
    }
    label::after{
      content: "*";
    }
    /*Untuk Form List*/
    button{
      background-color: #ff8811;
      width: 150px;
      height: 40px;
      /*border-radius: 5px;*/
      border:none;
      color:#ffffff;
      font-size:20px;
      /*margin-top: 10px;*/
      margin-bottom: 10px;
    }
    button:hover{
      background-color: #ff8844;
      color: #000000;
    }
  </style>
</head>
<body>
  <footer>
      <table align="center">
        <tr>
          <td align="center">
            <h4 align="center">Silahkan Comment Di Bawah ini............!!!</h4>
              <fieldset>
                <table align="center">
                  <tr>
                    <td>
                      <a target="_blank" title="find us on Facebook" href="http://www.facebook.com/">
                      <img alt="follow me on facebook" src="sosmed/facebook.png" border=0 width="30" height="30">
                      </a>
                      <a target="_blank" title="follow me on twitter" href="http://www.twitter.com/">
                        <img alt="follow me on twitter" src="sosmed/tumbler.png" border=0 width="30" height="30">
                      </a> 
                      <a target="_blank" title="follow me on instagram" href="http://www.instagram.com/">
                        <img alt="follow me on instagram" src="sosmed/instagram.png" border=0 width="30" height="30">
                      </a>
                      <a target="_blank" title="follow me on twitter" href="http://www.twitter.com/">
                        <img alt="follow me on twitter" src="sosmed/twitter.png" border=0 width="30" height="30">
                      </a>
                      <a target="_blank" title="find us on Gmail" href="http://www.gmail.com/">
                        <img alt="follow me on gmail" src="sosmed/gmail.png" border=0 width="30" height="30">
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>Email : <input type="email" name="email" size="30"></td>
                  </tr>
                  <tr>
                    <td>
                      <label>Comments</label> :
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <textarea name="comments" cols="100" rows="5" maxlength="200" placeholder="Masukkan Komentar Anda..."></textarea>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <fieldset>
                        <legend>Reaksi : </legend>
                          <input type="checkbox" value="suka" name="suka"> Suka
                          <input type="checkbox" value="menarik" name="menarik"> Menarik
                          <input type="checkbox" value="keren" name="keren"> Keren
                      </fieldset>
                  </td>
                  </tr>
                  <tr>
                    <td>
                      <fieldset>
                          <legend>Range : </legend>
                          <b>Penilaian Blog :</b>
                          <input type="range" name="range" min="1" max="100" step="1">
                      </fieldset>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href=""><button>Publish</button></a>
                    </td>
                  </tr>
                </table>
              </fieldset>
            <h5 align="center"><marquee behavior="alternate"> Thanks for your comment................................................................!!!</marquee></h5>
          </td>
        </tr>
      </table>
    </footer>
  <p align="center">CopyRight&copy2018 by : <i>Dwi Fitriadi Sukmawan</i></p>
</body>
</html>