<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Content</title>
  <link rel="stylesheet" href="">
  <style type="text/css">
    body{
      padding: 0;
      margin: 0;
      background-color: #5CC29F;
      height: 630px;
    }
    #content{
      /*background-image: url('bg.png');*/
      height: 400px;
      padding: none;
      /*background-size: cover;*/
    }
    content .sidebar{
      background: #651029;
      float: left;
      width: 20%;
      color: #fff;
      display: inline-block;
    }
    content .mainbar{
      /*background: #abcd64;*/
      float: auto;
      display: inline-block;
      overflow: hidden;
      width: 80%;
    }
    li a{
      color: #ffffff;
    }
    a:hover{
      color: #ff0000;
    }
    .info_side{
      background-color: #000fff;
      color: #ffffff;
      height: 50px;
      margin-left: 10px;
    }
    .info_side span{
      font-size: 20px;
      margin-left: 20px;
    }
    .list_article{
      width: 1000px;
      height: 550px;
      margin-left: 20px;
      margin-bottom: 20px;
      background-color: #ffffff;
      box-shadow: 5px 5px 10px black;
      border-radius: 5px;
      padding: 10px;
    }
    .list tr:nth-child(2n){
      background-color: #e4d3c1;
    }
    /*Untuk Form New Page*/
    .isi fieldset {
      border: 1px solid #DDDDDD;
      display: inline-block;
      font-size: 14px;
      padding: 2em 2em;
      margin-left: 100px;
    }
    .isi legend {
      background: #BFD48C;     
      color: #FFFFFF;          
      margin-bottom: 5px;
      padding: 0.5em 1em;
    }
   
    content .sub{
      width: 92%;
      height: 10%;
      margin-left: 50px;
      margin-right: 50px;
      margin-top: 50px;
      margin-bottom: 50px;
      background-color: #ffffff;
      display: absolute;
      border-radius: 10px;
      box-shadow: 5px 5px 5px #4e4e4e;
    }

    content .sub h1 {
        text-shadow: 2px 2px red;
        margin: 0;
        padding: 10px;
    }

    content .box{
      width: 91%;
      height: 1300px;
      margin-left: 50px;
      margin-right: 50px;
      margin-top: 50px;
      margin-bottom: 50px;
      background-color: #ffffff;
      display: absolute;
      border-radius: 10px;
      box-shadow: 5px 5px 5px #4e4e4e;
      padding: 10px;
    }

    ul.pagination {
      display: inline-block;
      padding: 0;
      margin: 0;
      position: absolute;
      right: 530px;
      top: 750px;
    }
    ul.pagination li {
      display: inline;
    }
    ul.pagination li a {
      color: black;
      float: left;
      padding: 8px 16px;
      text-decoration: none;
      background-color: #03DAC5;
      border: 2px solid #34bfa3;
      border-radius: 5px;
    }
    ul.pagination li.active a{
      border: 2px solid #000000;
    }
    ul.pagination li a:hover{
      border: 2px solid #000000;
    }

    .box table.scroll {
      width:100%;
      border:1px #a9c6c9 solid;
    }
    .box table.scroll thead {
      display:table;
      width:100%;
    }
    .box table.scroll tbody {
      display:block;
      height:250px;
      overflow:auto;
      float:left;
      width:100%;
    }
    .box table.scroll th, td {
      padding:8px;
    }

    .box table tbody tr td a {
      font-size: 15px;
      padding: 3px 5px;
      color: white;
      text-decoration: none;
      margin-top: -3px;
      display: inline-block;
    }

    .box table tbody tr td a {
      background-color: red;
    }

    .box table tbody tr td a:first-child {
      background-color: blue;
    }
    .box table tbody tr td a:hover {
      opacity: 0.5;
    }

    .box table.scroll {
        margin-top: 50px;
    }

    .no{
      width: 1px;
    }

    .box span{
     font-size: 20px;
     text-shadow: 1px 1px 1px #4f4f4f;
    }
  </style>
</head>
<body>
  <content>
      <div class="sidebar">
        <table border="1">
          <tr>
            <td class="info_side"><span>Devisi :</span></td>
          </tr>
          <tr>
            <td>
              <ul>
                <li><a href="#"> ALAT ANTRIAN </a></li>
                <li><a href="#"> SERBAMULTIMEDIA </a></li>
                <li><a href="#"> ALAT SKP </a></li>
                <li><a href="#"> BUSINESS DEVELOPMENT </a></li>
                <li><a href="#"> IT PROJECT </a></li>
                <li><a href="#"> MD PULSA </a></li>
                <li><a href="#"> E-GOVERNMENT </a></li>
              </ul>
            </td>
          </tr>
          <tr>
            <td class="info_side"><span>Lowongan :</span></td>
          </tr>
          <tr>
            <td>
              <ol>
                <li><a href="#">STAFF TEKNISI</a></li>
                <li><a href="#">PROGRAMMER ANDROID</a></li>
                <li><a href="#">STAFF MARKETING ONLINE</a></li>
                <li><a href="#">LOWONGAN SITE MANAGER</a></li>
                <li><a href="#">MANAGEMENT TRAINEE (MT) PROGRAMMER ERP & REGULER</a></li>
                <li><a href="#">PROGRAMMER FREELANCE</a></li>
              </ol>
            </td>
          </tr>
          <tr>
            <td class="info_side"><span>Client :</span></td>
          </tr>
          <tr>
            <td>
              Pilih Salah Satu :
              <select> 
                <option>Argumen 1</option>
                <option>Argumen 2</option>
                <option>Argumen 3</option>
                <option>Argumen 4</option>
                <option>Argumen 5</option>
              </select>
            </td>
          </tr>
          <tr>
            <td class="info_side"><span>Company Overview :</span></td>
          </tr>
          <tr>
            <td>
              <ol>
                <li><a href="">Company Overview</a></li>
                <li><a href="">VISI :</a></li>
                <li><a href="">MISI :</a></li>
                <li><a href="">VALUE</a></li>
                <li><a href="">STRUKTUR ORGANISASI</a></li>
                <li><a href="">MEET OUR BOD</a></li>
              </ol>
            </td>
          </tr>
          <tr>
            <td class="info_side"><span>Beberapa Client :</span></td>
          </tr>
          <tr>
            <td height="100">
              <ol>
                <li><a href="#">KLIEN DARI LEMBAGA / INSTANSI PEMERINTAH</a></li>
                <li><a href="#">KLIEN DARI LEMBAGA KESEHATAN</a></li>
                <li><a href="#">KLIEN DARI LEMBAGA KEUANGAN</a></li>
                <li><a href="#">KLIEN DARI LEMBAGA SWASTA</a></li>
              </ol>
            </td>
          </tr>
        </table>
      </div>
      <div class="mainbar">
          <article>
              <div class="list_article">
                <h1><a href="isi1.html">KEIN DORONG KEMAJUAN TEKNOLOGI INFORMASI</a></h1>
                  <p>
                    <b>KEMAJUAN TEKNOLOGI INFORMASI</b>
                  </p>
                  <hr />
                  <p>
                    Jakarta (ANTARA News) - Komite Ekonomi dan Industri Nasional (KEIN) akan mendorong kemajuan teknologi informasi untuk menumbuhkan perekonomian nasional. Menurut Ketua KEIN Soetrisno Bachir, ketersediaan infrastruktur yang lengkap akan meningkatkan kesejahteraan rakyat, maka salah satu infrastruktur yang penting saat ini adalah bidang teknologi informasi.
                  </p>
                  <img src="img/kein.jpg" alt="image light painting" width="500" height="300">
                  <h5>Autor by: <i>Dwi Fitriadi Sukmawan</i> Tanggal: 9/4/2018 </time></h5>
              </div>
              <div class="list_article">
                <h1><a href="isi2.html">6 Langkah Mudah Untuk Mendeteksi Website Penipuan</a></h1>
                  <p>
                    <b>Mendeteksi Website Penipuan</b>
                  </p>
                  <hr />
                  <p>
                    Suara.com - Internet ibarat pedang bermata dua. Jika tak hati-hati menggunakannya, Anda bisa terluka. Betapa tidak, selain berisi informasi-informasi berguna, internet kini berubah menjadi sarang penyamun. Salah sedikit, Anda bisa terpeleset jadi korban kejahatan seperti pencurian, pemerasan, dan penipuan. Banyak modus kejahatan di media internet, mulai dari media sosial, blog, hingga website abal-abal. Media terakhir ini merupakan yang terbanyak di internet.
                  </p>
                  <img src="img/website.jpg" alt="image panning" width="500" height="300">
                  <h5>Autor by: <i>Dwi Fitriadi Sukmawan</i> Tanggal: 9/4/2018 </h5>
              </div>
              <div class="list_article">
                <h1><a href="isi3.html">INGIN BELAJAR WEB PROGRAMMING, HARUS MULAI DARI MANA?</a></h1>
                  <p>
                    <b>INGIN BELAJAR WEB PROGRAMMING</b>
                  </p>
                  <hr />
                  <p>
                    HTML adalah inti dari seluruh halaman web. Sangat mustahil untuk membuat website tanpa memiliki dasar pengetahuan tentang HTML. Untungnya, HTML juga sangat mudah dipelajari. Anda tidak perlu memiliki dasar programming atau pengetahuan tentang algoritma apapun. Satu-satunya kemampuan yang dibutuhkan adalah anda sudah cukup familiar dengan cara penggunaan web browser seperti Google Chrome atau Mozilla Firefox.
                  </p>
                  <img src="img/css.jpg" alt="image makro" width="500" height="300">
                  <h5>Autor by: <i>Dwi Fitriadi Sukmawan</i> Tanggal: 9/4/2018 </h5>
                </div>
          </article>
      </div>
    </content>
</body>
</html>