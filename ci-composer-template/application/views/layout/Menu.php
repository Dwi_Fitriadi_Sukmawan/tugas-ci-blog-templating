 <!DOCTYPE html>
 <html>
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <title>Menu</title>
   <link rel="stylesheet" href="">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
 <style type="text/css">
  #menu{
    /*background-color: #4E4E4E;*/
    background: linear-gradient(#687587, #404853);
    height: 90px;
    margin-top: 0px;
    margin-left: -40px;
  }
  nav{
    height: 100px;
    line-height: 90px;
    position: relative;
    /*margin-left: 644px;*/
  }
  nav ul{
    list-style: none;
  }
  nav ul li a{
    float: left;
    border-left: 1px solid rgba(0, 0, 0, 0.2);
    border-right: 1px solid rgba(255, 255, 255, 0.1);
    color: white;
    font-variant: small-caps;
    text-decoration: none;
    padding: 0px 30px;
  }
  nav li a:hover{
    background: #454d59;
    color: #d0d2d5;
  }
  #a{
    background: #454d59;
    color: #d0d2d5;
  }
  nav li a img{
    margin-top: 5px;
    width: 80px;
  }
  /*css untuk menu hover dropdown*/
  nav li:hover .dropdown{
    display: block;
  }
  nav li .dropdown{
      display:none;
      position:absolute;
      left:1191px;
      top:90px;
      background: linear-gradient(#687587, #404853);
      width: 158px;
      border: none;
      outline: none;
      padding: 0;
  }
  .dropdown a{
    width: 100px;
    height: 80px;
  }
  .dropdown a:hover{
    color: #fff;
  }
  .logo{
    width: 300px;
    height: 90px;
    float: left;
  }
  </style>
  </head>
  <body>
    <menu>
      <div id="menu">
        <nav>
          <ul>
            <li><a href="index.html" style="margin-left: 250px;"><i class="fas fa-home"></i> HOME</a></li>
            <li><a href="#"><i class="fas fa-check"></i> ABOUT</a></li>
            <li><a href="#"><i class="fas fa-users"></i> USER</a></li>
            <li><a href="#"><i class="fas fa-user"></i> PROFIL</a></li>
            <li><a href="#"><i class="far fa-address-book"></i> CONTACT</a></li>
          </ul>
          <ul>
            <li style="float:right">
              <a href="#" style="height: 90px;"><img src="img/avatar.png" width="15">
                <i class="fas fa-caret-down"></i>
              </a>
              <ul class="dropdown">
                <li><a href="#"><i class="fas fa-user"></i> Profil</a></li>
                <li><a href="#"><i class="fas fa-key"></i> Password</a></li>
                <li><a href="login.html"><i class="fas fa-sign-in-alt"></i> Login</a></li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </menu>
  </body>
  </html>